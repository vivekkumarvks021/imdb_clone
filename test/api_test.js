let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
//let fs = require('fs');
let expect = chai.expect;
let should= chai.should();


chai.use(chaiHttp);
//Our parent block
describe('IMDb_clone', () => {

    describe('/POST Login', () => {
        it('login successful test', (done) => {
            let login_data = {
                username: "raahul410",
                password: "$2y$12$rT/jAq5Mi5j2c4zhGwMtyewZ5aDCmFeQUGrlrSxwbjVEzKcP1cHtG"
            }
            chai.request(server)
            .post('/login')
            .send(login_data)
            .end((err, res) => {
                    res.should.have.status(200);
                    //console.log(res.body);
                    res.body.should.be.a('object');
                done();
            });
        });

        it('wrong username test', (done) => {
            let login_data = {
                username: "raahul410434343",
                password: "$2y$12$rT/jAq5Mi5j2c4zhGwMtyewZ5aDCm"
            }
            chai.request(server)
            .post('/login')
            .send(login_data)
            .end((err, res) => {
                    res.should.have.status(200);
                    //console.log(res.body);
                    res.body.should.be.a('object');
                done();
            });
        });

        it('wrong password test', (done) => {
            let login_data = {
                username: "raahul410",
                password: "$2y$12$rT/jAq5Mi5j2c4zhGwMtyewZ5aDCm"
            }
            chai.request(server)
            .post('/login')
            .send(login_data)
            .end((err, res) => {
                    res.should.have.status(200);
                    //console.log(res.body);
                    res.body.should.be.a('object');
                done();
            });
        });
    });

    describe('/POST Register', () => {
        it('register successful test', (done) => {
            let register_data = {
                username: "raahul4",
                password: "$2y$12$rT/jAq5Mi5j2c4zhGwMtyewZ5aDCmFeQUGrlrSxwbjVEzKcP1cHtG",
                email: "rs@gmail.com"
            }
            chai.request(server)
            .post('/register')
            .send(register_data)
            .end((err, res) => {
                    res.should.have.status(200);
                    //console.log(res.body);
                    res.body.should.be.a('object');
                done();
            });
        });

        it('existing username test', (done) => {
            let register_data = {
                username: "raahul4",
                password: "$2y$12$rT/jAq5Mi5j2c4zhGwMtyewZ5aDCmFeQUGrlrSxwbjVEzKcP1cHtG",
                email: "rskumar@gmail.com"
            }
            chai.request(server)
            .post('/register')
            .send(register_data)
            .end((err, res) => {
                    res.should.have.status(200);
                    //console.log(res.body);
                    res.body.should.be.a('object');
                done();
            });
        });

        it('registered email test', (done) => {
            let login_data = {
                username: "raahulvfv",
                password: "$2y$12$rT/jAq5Mi5j2c4zhGwMtyewZ5aDCm",
                email: "raahul410@gmail.com"
            }
            chai.request(server)
            .post('/register')
            .send(login_data)
            .end((err, res) => {
                    res.should.have.status(200);
                    //console.log(res.body);
                    res.body.should.be.a('object');
                done();
            });
        });
    });

    describe('/POST Authentication', () => {
        it('sent correct token of username raahul410, username is not dynamic in code.', (done) => {
            let token ={token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InJhYWh1bDQxMCIsInBhc3N3b3JkIjoiJDJ5JDEyJHJUL2pBcTVNaTVqMmM0emhHd010eWV3WjVhRENtRmVRVUdybHJTeHdialZFektjUDFjSHRHIiwiaWF0IjoxNTgwMjg0MTUwfQ.6O-UB8HHnP8WJPdfJnNC941sllt8JoUGcJ6sfLmd35k"} ;
            chai.request(server)
            .post('/login-authentication')
            .send(token)
            .end((err, res) => {
                    res.should.have.status(200);
                    //console.log(res.body);
                    res.body.should.be.a('object');
                done();
            });
        });

        it('send wrong token of username raahul410', (done) => {
                let token = {token:"hbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InJhYWh1bDQxMCIsInBhc3N3b3JkIjoiJDJ5JDEyJHJUL2pBcTVNaTVqMmM0emhHd010eWV3WjVhRENtRmVRVUdybHJTeHdialZFektjUDFjSHRHIiwiaWF0IjoxNTgwMjg0MTUwfQ.6O-UB8HHnP8WJPdfJnNC941sllt8JoUGcJ6sfLmd35k"};
                chai.request(server)
                .post('/login-authentication')
                .send(token)
                .end((err, res) => {
                        res.should.have.status(200);
                        //console.log(res.body);
                        res.body.should.be.a('object');
                    done();
                });
        });

        
    });

    describe('/GET latest movie', () => {
        it('Fetch Latest movie in the Server', (done) => {
            chai.request(server)
            .get('/get_latest_movie')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(1);
                expect(res.body).to.be.an('array');
                //console.log(res.body);
                done();
              });
        });
    });

    describe('/POST movie by genre', () => {
        it('Movie by Genre Which is Matching', (done) => {
            let genre= {genre:"Adventure"};
            chai.request(server)
            .post('/genre')
            .send(genre)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                //res.body.length.should.be.eql(1);
                //expect(res.body).to.be.an('array');
                //console.log(res.body);
                done();
              });
        });

        it('Movie if Genre is Not Matching', (done) => {
            let genre= {genre:"Adventu"};
            chai.request(server)
            .post('/genre')
            .send(genre)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                //res.body.length.should.be.eql(1);
                //expect(res.body).to.be.an('array');
                //console.log(res.body);
                done();
              });
        });
    });

    describe('/POST movie by actor id', () => {
        it('Movie by single actor id Which is Matching', (done) => {
            let actor= {actor:"5e295b5b093bf31004b7c5c2"};
            chai.request(server)
            .post('/get_actor_movie')
            .send(actor)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                //console.log(res.body);
                done();
              });
        });

        it('Movie if Actor id is Not Matching', (done) => {
            let actor= {actor:"5e295b5b093bf31004b7c5c2vjhvjhvjh"};
            chai.request(server)
            .post('/get_actor_movie')
            .send(actor)
            .end((err, res) => {
                res.should.have.status(200);
                //console.log(res.body);
                done();
              });
        });
    });

    describe('/POST Comment on movie', () => {
        it('Sending comment on movie', (done) => {
            let comment= {
                username:"raahul410",
                comment:"Awesome Comment on Bharat Movie",
                movie: "Bharat"
            };
            chai.request(server)
            .post('/comment')
            .send(comment)
            .end((err, res) => {
                res.should.have.status(200);
                //res.body.should.be.a('array');
                console.log(res.body);
                done();
            });
        });

    });

    describe('/POST Rating of movie', () => {
        it('rate by user Who already rated this movie', (done) => {
            let rate= {
                username:"raahul410",
                rate: 6,
                movie: "Bharat"
            };
            chai.request(server)
            .post('/rating')
            .send(rate)
            .end((err, res) => {
                res.should.have.status(200);
               // res.body.should.be.a('array');
                console.log(res.body);
                done();
              });
        });

        it('Rating of movie', (done) => {
            let rate= {
                username:"raahul41011",
                rate: 6,
                movie: "Bharat"
            };
            chai.request(server)
            .post('/rating')
            .send(rate)
            .end((err, res) => {
                res.should.have.status(200);
               // res.body.should.be.a('array');
                console.log(res.body);
                done();
              });
        });

        it('Rating of movie if movie not found in database', (done) => {
            let rate= {
                username:"raahul410",
                rate: 6,
                movie: "Bharati"
            };
            chai.request(server)
            .post('/rating')
            .send(rate)
            .end((err, res) => {
                res.should.have.status(200);
               // res.body.should.be.a('array');
                console.log(res.body);
                done();
              });
        });
    });

    describe('/POST Add movie details', () => {
        it('Add movie details', (done) => {
            
            chai.request(server)
            .post('/add_movie')
            .set('Content-Type', 'application/form-data')
            .field('name','Race 2')
            .field('description','hello movie')
            .field('genre','Action')
            .field('cast','Salman Khan')
            .field('release_date','2020-02-01')
            .attach('file', '/Users/Public/Pictures/download.jpg','download.jpg')
            .end((err, res) => {
                res.should.have.status(200);
                //res.body.should.be.a('array');
                console.log(res.body);
                done();
            });
        });

    });

    describe('/POST Add Actor details', () => {
        it('Add Actor details', (done) => {
            
            chai.request(server)
            .post('/add_actor')
            .set('Content-Type', 'application/form-data')
            .field('name','Salmaan Khan')
            .field('age',57)
            .field('movie','Race 2')
            .field('link','Salman Khan')
            .attach('file', '/Users/Public/Pictures/download.jpg','download.jpg')
            .end((err, res) => {
                res.should.have.status(200);
                //res.body.should.be.a('array');
                //console.log(res.body);
                done();
            });
        });

    });

});