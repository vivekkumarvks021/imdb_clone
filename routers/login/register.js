var jwt = require('jsonwebtoken');
var login_model = require('../../Models/login')

exports.register = function(req,res,key){
    //res.json(req.body);
    var register= new login_model({
        username : req.body.username,
        password : req.body.password,
        email : req.body.email  
    })
    
    login_model.count({$or:[{email:req.body.email},{username:req.body.username}]},function(err,count){
        if(err){
            res.json({
                status:false,
                msg:"Error in register"
            })
        }else if(count>0){
            res.json({
                status:false,
                msg:"user already registered"
            })
        }else if(count == 0){
            register.save((err,data)=>{
                if(err){
                    res.json({
                        status:false,
                        msg:"Error in registering"
                    })
                }else{
                    res.json({
                        status:true,
                        msg:"registered successfully"
                    })
                }
            })
        }
    })
}