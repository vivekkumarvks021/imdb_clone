var jwt = require('jsonwebtoken');
var login_model = require('../../Models/login')

exports.login = function(req,res,key){
    
    login_model.find({username:req.body.username},{})
    .exec(function(err, data){
        if(err){
            res.json({
                status:false,
                msg: "login error"
            });
        }else if(data.length==0){
            res.json({
                status:false,
                msg : "username not found, Please register first"
            })
        }else if(data[0].password != req.body.password){
            res.json({
                status:false,
                msg : "password didn't matched"
            })
        }else{
            var head ={
                username: data[0].username,
                password: data[0].password
            };
            var token = jwt.sign(head,'ADVDVD12121');
            res.json({
                status:true,
                msg : "login successful",
                token:token
            })
        }
    })
}