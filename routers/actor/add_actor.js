var multer = require('multer');
let jwt = require('jsonwebtoken');
var actor_detail = require('../../Models/actors');
var store = multer.diskStorage({

  destination: function (req, file, cb) {
    cb(null,'./uploads')
  },
  filename: function (req, file, cb) 
  { cb(null, file.fieldname+'-'+Date.now()+file.originalname);}

});

var upload = multer({storage: store}).single('file');

exports.add_actor = function(req,res,key){
    upload(req,res,function(err){
        if(!req.body.token){
          res.json({
            status:false,
            msg: "token is required"
          })
        }else{
          let token =req.body.token;
            jwt.verify(token,'ADVDVD12121',function(err,data){
                if(err){
                    res.json({
                        status:false,
                        msg: "User not verified"
                    })
                }else if(data.username=='raahul410'){
                  var actor_data = new actor_detail({
                    name : req.body.name,
                    age:req.body.age,
                    photo : req.file.filename,
                    movie_worked : req.body.movie,
                    linkofmovie :req.body.link
                });
                //res.json(req.body.link);
    
                actor_data.save((err,data)=>{
                  if(err){
                    res.json({
                      status: false,
                      msg: "Error while adding actor details"
                    })
                  }else{
                    res.json({
                      status: true,
                      msg: "Actor Added Successfully"
                    })
                  }
                })
                }
                
            });
        }
       // }
    })
}