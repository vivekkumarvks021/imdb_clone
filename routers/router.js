const express = require('express');

var router = express.Router();

var tokenVerify = require('./login/api_auth').api_auth;

var add_movie = require('./movie/add_movie');
router.post('/add_movie',add_movie.add_movies);

var add_actor = require('./actor/add_actor');
router.post('/add_actor',add_actor.add_actor);

var get_actor_movie = require('./movie/get_movie');
router.post('/get_actor_movie',tokenVerify, get_actor_movie.actor_movies);

var latest_movie = require('./movie/latest_movie');
router.get('/get_latest_movie', latest_movie.latest_movvie);

var login = require('./login/login');
router.post('/login',login.login);

var login_auth = require('./login/login_auth');
router.post('/login-authentication',login_auth.login_auth);

var register = require('./login/register');
router.post('/register',register.register);

var movie_according_to_genre = require('./movie/m_acc_to_genre');
router.post('/genre', tokenVerify, movie_according_to_genre.movie_acc_genre);

var search_movie = require('./movie/search_movie');
router.post('/search_movie', tokenVerify, search_movie.search_movie);

var comment = require('./movie/comment');
router.post('/comment', tokenVerify, comment.comment);

var rating = require('./movie/rating');
router.post('/rating',tokenVerify,rating.rating);

module.exports = router;