var movie = require('../../Models/movies');

exports.movie_acc_genre = function(req,res,key){

    var genre = req.body.genre;
    movie.find({genre:genre},{name:1}).exec(function(err,data){
        if(err){
            res.json({
                status: false,
                msg:"Error in Finding Movie"
            })
        }else if(data.length==0){
            res.json({
                status:false,
                msg: "Movie not found of this type of genre"
            });
        }else{
            res.json(data);
        }
    })

}