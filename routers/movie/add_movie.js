var multer = require('multer');
var movie_detail = require('../../Models/movies');
let jwt = require('jsonwebtoken');
var actor = require('../../Models/actors');
var store = multer.diskStorage({

  destination: function (req, file, cb) {
    cb(null,'./uploads')
  },
  filename: function (req, file, cb) 
  { cb(null, file.fieldname+'-'+Date.now()+file.originalname);}

});

var upload = multer({storage: store}).any('file');

exports.add_movies = function(req,res,key){
    upload(req,res,function(err){
        if(!req.body.token){
            res.json({
              status:false,
              msg: "token is required"
            })
        }else{

            let token =req.body.token;
            jwt.verify(token,'ADVDVD12121',function(err,data){
                if(err){
                    res.json({
                        status:false,
                        msg: "User not verified"
                    })
                }else if(data.username=='raahul410'){
                    var image_list=[] ;
        req.files.map((x)=>{
        image_list.push({image:x.filename}) ;
                
        });

        if(image_list.length>6){
            res.json({
                status: false,
                msg: 'You can upload maximum 6 image once'
            });
        }else{
            var movie_data = new movie_detail({
                name : req.body.name,
                description:req.body.description,
                genre : req.body.genre,
                release_date : req.body.release_date,
                movie_image :image_list,
                cast:req.body.cast
            });
            

            movie_data.save((err,data)=>{
                if(err) {
                    res.status(401).json({
                        status: false,
                        msg: 'Movie details is not added'
                    })
                } ;
                res.status(200).json({
                    status: true,
                    msg: 'movie details is added successfully'
                });
            })
        }
                }
                
            });

        }
    })
}