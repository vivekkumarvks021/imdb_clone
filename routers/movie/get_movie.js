var movie_detail = require('../../Models/movies');


exports.actor_movies = function(req,res,key){
    var actor_id = req.body.actor;
    movie_detail.aggregate([
        {$match:{cast:{$in:[actor_id]}}},
        {$group:{"_id":"$_id","name":{$first:'$name'},count:{$sum:1}}}
    ]).exec((err,data)=>{
        if(err){
            res.status(401).json({
                status:false,
                msg: "Error in searching movie"
            });
        }else if(data.length==0){
            res.json({
                status:false,
                msg: "Movie not found of this actor"
            });
        }else{
            res.status(200).json(data);
        }
    })
}