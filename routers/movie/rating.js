var movie = require('../../Models/movies');

exports.rating = function(req,res){

    var new_rating = {username:global.username,user_rate:parseInt(req.body.rate)};
    movie.countDocuments(
        {"mrate_details.username":username,
        name:req.body.movie})
    .exec(function(err,data){
        if(data>0){
            res.json({status:false,msg:'You can rate Once a movie'})
        }else{
            movie.find(
                {name:req.body.movie},
                {mrate:1,mrate_details:1})
            .limit(1)
            .exec(function(error,mdata){
                if(mdata!=0){
                    var new_rate = parseFloat(mdata[0].mrate)+parseFloat(req.body.rate);
                new_rate = new_rate/2;
                //console.log(new_rate);
            
                movie.findOneAndUpdate(
                    {name:req.body.movie},
                    {$push:{mrate_details:new_rating},$set:{mrate:new_rate}},
                    ).exec((err,data)=>{
                        if(err){
                            res.json({
                                status: false,
                                msg: "Error While Rating"
                            })
                        }else{
                            res.json({
                                status: true,
                                msg: "Rated Successfully"
                            })
                        }
                    });
                }else{
                    res.json({
                        status:false,
                        msg: 'movie not found'
                    })
                }
                
            });
            
        }
    });
}