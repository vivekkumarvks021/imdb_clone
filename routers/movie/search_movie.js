var movie = require('../../Models/movies');

exports.search_movie= function(req,res,key){
    var movie_name= req.body.m_name;
    var actor_name= req.body.a_name;

    movie.find({
        $or:[
            {name:{ $regex: '.*' + movie_name + '.*' }},
            {cast:actor_name}
        ]
    },{name:1})
    .exec((err,data)=>{
        if(err){
            res.json({
                status: false,
                msg: "Error while searching"
            });
        } else{
            res.json(data);
        }
    })
}