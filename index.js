var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var routers = require('./routers/router');

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/imdb_clone', ()=>{
    console.log("Database connected")
});




var app = new express();
app.use(bodyParser.json());
app.use(bodyParser({ uploadDir: path.join(__dirname, 'files'), keepExtensions: true }));

app.use('/', routers);

module.exports=app.listen( 2000, function(){ console.log('listening on port 2000'); } );