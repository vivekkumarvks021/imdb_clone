const mongoose = require('mongoose');
//var actor_details = require('./actors');
var Schema = mongoose.Schema;

var movies = new Schema({
    name:String,
    movie_image:[],
    description:String,
    genre:String,
    cast:[],
    release_date:Date,
    comment:[],
    mrate:Number,
    mrate_details:[]
});

module.exports = mongoose.model('movie_detail',movies);