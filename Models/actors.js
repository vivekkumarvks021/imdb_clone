const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var actors = new Schema({
    name:String,
    age:Number,
    photo:String,
    movie_worked:{
        movie1:String,
        movie2:String,
        movie3:String
    },
    linkofmovie:{
        link1:String,
        link2:String,
        link3:String
    }
});

module.exports = mongoose.model('actor_detail',actors);