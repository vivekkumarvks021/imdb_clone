const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var login = new Schema({
    username : String,
    password : String,
    email : String
})

module.exports = mongoose.model('login',login);