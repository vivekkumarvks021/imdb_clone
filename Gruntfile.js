module.exports= function(grunt){
    grunt.initConfig({
        concat:{
            js:{
                src:['index.js','routers/*.js','routers/actor/*.js','routers/login/*.js','routers/movie/*.js'],
                dest: 'build/script.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat')
     
    grunt.registerTask('run', ['concat:js']);
};